# Git Field Manual

Welcome to the Git Field Manual, a comprehensive guide designed to help developers understand and effectively use Git. This manual is organized into three main sections: Concepts, Commands, and Troubleshooting. Each section aims to provide concise, actionable information that can fit on a single page, making it easy to reference and use in your daily workflow.

It is inspired by the Red Team Field Manual, a popular guide for cybersecurity professionals, and aims to provide a similar level of utility and accessibility for Git users. Whether you're new to Git or a seasoned pro, this manual has something for everyone.

![Git Field Manual](images/cover.jpg)

## Technical Stuff

The Book is written and created using the [easy book generator](https://gitlab.com/sigmund.dominik/easy-book-generator)

## Contributing

We appreciate and welcome contributions from the community to enhance the features and overall quality of this book. Whether you're a developer, tester, or enthusiastic user, there are several ways you can contribute:

### Creating Issues

If you encounter a typo, have a chapter request, or want to suggest improvements, please [create an issue](https://gitlab.com/sigmund.dominik/git-field-manual/-/issues/new) on our Gitlab repository. When creating an issue, provide detailed information about the problem or enhancement you're addressing. This includes any relevant context that can help our team understand and address it effectively.

### Merge Requests

If you'd like to contribute text, documentation, or fixes, we encourage you to submit a pull request. Before creating a pull request, please:

1. Fork the repository.
2. Create a new branch for your changes.
3. Make your modification
4. Update this README if necessary
5. Submit a merge request to the `main` branch of the repository.

We'll review your merge request, provide feedback, and work with you to ensure that your contribution aligns with the project's goals and standards.

## License

The Git Field Manual is licensed under the [MIT License](LICENSE.md). Feel free to use, modify, and distribute it according to your needs.

## Authors

- **[Dominik Sigmund](https://gitlab.com/sigmund.dominik)** - Lead Author and Developer

## Acknowledgments

We appreciate the collaborative efforts that have contributed to the success of the Git Field Manual. We'd like to thank the following individuals and organizations for their support and contributions:

- (none yet, but you could be the first!)

## Support

If you can't find a solution to your issue in the documentation, feel free to reach out to us for assistance. We offer support through the following channels:

- **Email:** For non-urgent matters or if you prefer email communication, you can reach us at [gfm@webdad.eu](mailto:gfm@webdad.eu). Please provide detailed information about your issue so that we can assist you more effectively.

## Important Note

When seeking support, make sure to include the following information in your message:

1. A detailed description of the issue you're facing.
2. Steps to reproduce the problem (if applicable).
3. Relevant configuration details.
4. Any error messages or logs related to the issue.

This information will help us understand your situation better and provide a more accurate and timely response.

Thank you for choosing GFM!! We're committed to ensuring you have a positive experience, and we appreciate your cooperation in following these support guidelines.
