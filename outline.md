# Outline

## Table of Contents

| Part                | Chapter                                 |
|---------------------|-----------------------------------------|
| **Concepts**        | 1. Repositories                         |
|                     | 3. Commits                              |
|                     | 4. Branches                             |
|                     | 5. Merging                              |
|                     | 6. Conflict Resolution                  |
|                     | 7. Rebasing                             |
|                     | 8. Remote Repositories                  |
|                     | 9. Tags                                 |
|                     | 10. Cherry-Picking                      |
|                     | 11. Submodules                          |
|                     | 12. Hooks                               |
|                     | 13. Git Flow                            |
|                     | 14. Bisect                              |
|                     | 15. Blame                               |
|                     | 16. Archive                             |
| **Commands**        | 1. git init                             |
|                     | 2. git clone                            |
|                     | 3. git status                           |
|                     | 4. git add                              |
|                     | 5. git commit                           |
|                     | 6. git log                              |
|                     | 7. git diff                             |
|                     | 8. git branch                           |
|                     | 9. git checkout                         |
|                     | 10. git merge                           |
|                     | 11. git rebase                          |
|                     | 12. git fetch                           |
|                     | 13. git pull                            |
|                     | 14. git push                            |
|                     | 15. git tag                             |
|                     | 16. git reset                           |
|                     | 17. git revert                          |
|                     | 18. git stash                           |
|                     | 19. git remote                          |
|                     | 20. git config                          |
| **Troubleshooting** | 1. Remove sensitive data                |
|                     | 2. Undo a push                          |
|                     | 3. Move commit to correct branch        |
|                     | 4. Change last commit message           |
|                     | 5. Resolve merge conflicts              |
|                     | 6. Delete a remote branch               |
|                     | 7. Undo a merge                         |
|                     | 8. Recover a deleted branch             |
|                     | 9. Fix a rebase                         |
|                     | 10. Squash commits                      |
|                     | 11. Use Git Reflog to Recover from Mistakes|
|                     | 12. Amend a Commit                      |
|                     | 13. Change Commit Message               |
|                     | 14. Move Commits to a New Branch        |
|                     | 15. Undo Last Commit                    |
|                     | 16. Fix `git diff` Showing No Changes   |
|                     | 17. Revert a Commit from Several Commits Ago|
|                     | 18. Undo Changes to a File              |
|                     | 19. Completely Reset Local Changes      |

## Part 1: Concepts

### 1. Repositories
- Definition and purpose
- Local vs Remote repositories
- Initializing a repository

### 2. Stages
- Working Directory, Staging Area, and Repository
- Adding files to the staging area
- Viewing the staging area

### 3. Commits
- What is a commit?
- Creating commits
- Commit messages
- Amending commits

### 4. Branches
- Definition and purpose
- Creating and deleting branches
- Merging branches
- Branching strategies

### 5. Merging
- Fast-forward merges
- 3-way merges
- Merge conflicts

### 6. Conflict Resolution
- Types of conflicts (merge conflicts, rebase conflicts)
- Strategies for resolving conflicts
- Using tools for conflict resolution

### 7. Rebasing
- What is rebasing?
- Interactive rebase
- Rebase vs merge

### 8. Remote Repositories
- Cloning
- Fetching and pulling
- Pushing changes
- Tracking branches

### 9. Tags
- Annotated vs lightweight tags
- Creating and listing tags
- Tagging releases

### 10. Cherry-Picking
- What is cherry-picking?
- Use cases for cherry-picking
- How to cherry-pick a commit

### 11. Submodules
- What are submodules?
- Adding, updating, and removing submodules
- Best practices for using submodules

### 12. Hooks
- What are Git hooks?
- Client-side vs server-side hooks
- Common hooks (pre-commit, post-commit, pre-push, etc.)

### 13. Git Flow
- Overview of Git Flow
- Setting up Git Flow
- Workflow steps (feature branches, release branches, hotfix branches)

### 14. Bisect
- What is git bisect?
- How to use git bisect to find bugs
- Practical examples of using git bisect

### 15. Blame
- What is git blame?
- How to use git blame
- Practical applications of git blame

### 16. Archive
- Creating an archive of a repository
- Using git archive

## Part 2: Commands

### 1. git init
- Initialize a new repository

### 2. git clone
- Clone an existing repository

### 3. git status
- Check the status of the working directory

### 4. git add
- Add files to the staging area

### 5. git commit
- Commit changes

### 6. git log
- View commit history

### 7. git diff
- Show changes between commits, branches, etc.

### 8. git branch
- List, create, or delete branches

### 9. git checkout
- Switch branches or restore working directory files

### 10. git merge
- Merge branches

### 11. git rebase
- Reapply commits on top of another base tip

### 12. git fetch
- Download objects and refs from another repository

### 13. git pull
- Fetch and integrate with another repository or a local branch

### 14. git push
- Update remote refs along with associated objects

### 15. git tag
- Create, list, delete, or verify a tag object

### 16. git reset
- Reset current HEAD to the specified state

### 17. git revert
- Revert some existing commits

### 18. git stash
- Stash the changes in a dirty working directory away

### 19. git remote
- Manage set of tracked repositories

### 20. git config
- Get and set repository or global options

## Part 3: Troubleshooting

### 1. Remove sensitive data
- Steps to remove sensitive data from history

### 2. Undo a push
- Undoing the push

### 3. Move commit to correct branch
- Moving commits to the correct branch

### 4. Change last commit message
- Amending the most recent commit

### 5. Resolve merge conflicts
- Resolving merge conflicts

### 6. Delete a remote branch
- Deleting a branch from the remote repository

### 7. Undo a merge
- Reverting a merge commit

### 8. Recover a deleted branch
- Recovering deleted branches

### 9. Fix a rebase
- Aborting or continuing a rebase

### 10. Squash commits
- Squashing commits into a single commit

### 11. Use Git Reflog to Recover from Mistakes
- Using `git reflog` to view the history of all operations and reset to a previous state

### 12. Amend a Commit
- Amending the last commit to include small changes or fix mistakes

### 13. Change Commit Message
- Changing the message of the last commit using `git commit --amend`

### 14. Move Commits to a New Branch
- Creating a new branch from the current state and removing the last commit from the current branch

### 15. Undo Last Commit
- Undoing the last commit but keeping the changes using `git reset --soft HEAD~`

### 16. Fix `git diff` Showing No Changes
- Using `git diff --staged` to show changes that have been added to the staging area

### 17. Revert a Commit from Several Commits Ago
- Using `git revert` to create a new commit that undoes changes made by a previous commit

### 18. Undo Changes to a File
- Reverting a specific file to a previous commit using `git checkout [commit_hash] -- path/to/file`

### 19. Completely Reset Local Changes
- Resetting the local repository to match the remote repository using `git fetch origin`, `git reset --hard origin/master`, and `git clean -d --force`
