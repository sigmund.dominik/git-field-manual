---
tag: done
---

# Remove a File from the Remote Repository

To remove a file from a remote repository, simply delete the file locally, commit the change, and push the changes to the remote repository. This ensures that the file is removed from both the local and remote repositories.

**Remove the File Locally:**  
Delete the file from your working directory:  
`rm <file>`

**Stage the Deletion:**  
Stage the deletion of the file:  
`git add <file>`

**Commit the Changes:**  
Commit the deletion:  
`git commit -m "Remove <file>"`

**Push the Changes:**  
Push the changes to the remote repository:  
`git push origin <branch>`
