---
tag: done
---

# Fix `git diff` Showing No Changes

Sometimes, you may run `git diff` expecting to see changes, but it shows no output even though there are modifications in your working directory. This issue can occur for various reasons, such as changes not being staged, incorrect usage of the command, or issues with line endings.

**Check for Unstaged Changes:**  
Ensure that the changes are not already staged:  
`git diff --staged`

**Check the Working Directory:**  
Verify that there are actual changes in the working directory:  
`git status`

**Check for Line Ending Issues:**  
Ensure that line ending differences are not being ignored. Configure Git to handle line endings consistently:  
`git config --global core.autocrlf true  # For Windows`  
`git config --global core.autocrlf input # For Unix/Linux`
