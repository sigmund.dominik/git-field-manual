---
tag: done
---

# Squash Commits

Squashing commits allows you to combine multiple commits into a single cohesive commit. This is useful for cleaning up your commit history before merging branches or sharing your work with others. Squashing commits can be done interactively using Git's rebase command.

**Start an Interactive Rebase:**  
Begin an interactive rebase for the last n commits:  
`git rebase -i HEAD~n`

**Mark Commits to Squash:**  
In the text editor that opens, change the command for the commits you want to squash from `pick` to `squash` or `s`. Leave the first commit as `pick`.

**Edit Commit Messages:**  
Edit the commit messages as needed. You can combine messages or write a new message for the squashed commit.

**Complete the Rebase:**  
Save and close the editor to complete the rebase.
