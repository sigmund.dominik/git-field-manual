---
tag: done
---

# Completely Reset Local Changes

If you need to discard all local changes and reset your working directory to match the last commit in the branch, you can use the `git reset` and `git checkout` commands. This is useful when you want to abandon all uncommitted changes and start fresh from the current HEAD commit.

**Reset the Staging Area and Working Directory:**  
Use the following command to reset both the staging area and the working directory:  
`git reset --hard HEAD`

**Clean Untracked Files:**  
Remove all untracked files and directories:  
`git clean -fd`
