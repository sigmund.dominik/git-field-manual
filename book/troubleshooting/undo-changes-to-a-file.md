---
tag: done
---

# Undo Changes to a File

Sometimes, you will need to undo changes to a specific file and revert it to the state it was in a previous commit. Git provides a simple way to do this using the `git checkout` command.

**Identify the Commit:**  
Use the following command to find the commit hash:  
`git log`

**Checkout the Specific File:**  
Revert the file to the state it was in the specified commit:  
`git checkout <commit-hash> -- <path/to/file>`

**Stage the Reverted File:**  
Stage the file to include it in the next commit:  
`git add <path/to/file>`

**Commit the Changes:**  
Commit the reverted file:  
`git commit -m "Revert <path/to/file> to a previous commit"`
