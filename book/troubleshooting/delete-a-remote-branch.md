---
tag: done
---

# Delete a Remote Branch

When cleaning up outdated or merged branches in your repository, deleting a remote branch is a common task. This helps keep the remote repository organized and reduces clutter.

**List Remote Branches:**  
To see a list of remote branches, run:  
`git branch -r`

**Delete the Remote Branch:**  
To delete a remote branch, use:  
`git push origin --delete <branch-name>`

**Verify the Deletion:**  
To confirm that the branch has been deleted, list the remote branches again:  
`git branch -r`
