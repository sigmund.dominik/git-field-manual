---
tag: done
---

# Change Last Commit Message

Sometimes, you'll realize that the message of your last commit needs to be corrected or improved. Git lets you change the last commit message with the `git commit --amend` command. This process updates the commit message without altering the commit hash if no other changes are made.

**Amend the Last Commit:**  
Run the following command to amend the last commit message:  
`git commit --amend`

**Edit the Commit Message:**  
Your default text editor will open with the current commit message. Modify the message as needed and save the changes.

**Force Push the Amended Commit:**  
If the commit has already been pushed to a remote repository, force push the changes:  
`git push origin <branch> --force`
