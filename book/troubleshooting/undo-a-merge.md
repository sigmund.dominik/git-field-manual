---
tag: done
---

# Undo a Merge

Undoing a merge is necessary when you realize that the merge introduced errors or conflicts that you want to revert. There are different methods to undo a merge depending on whether the merge was committed or not.

**Steps to Undo a Committed Merge:**

**Find the Merge Commit:**  
Identify the merge commit hash using:  
`git log`

**Revert the Merge Commit:**  
Revert the merge commit using:  
`git revert -m 1 <merge-commit-hash>`

**Resolve Any Conflicts:**  
If there are conflicts, resolve them and then commit the changes:  
`git commit -m "Revert merge"`

**Steps to Undo an Uncommitted Merge:**

**Abort the Merge:**  
Abort the merge process using:  
`git merge --abort`
