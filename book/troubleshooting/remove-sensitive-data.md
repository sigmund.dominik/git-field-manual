---
tag: done
---

# Remove Sensitive Data

It is possible for sensitive data such as passwords or API keys to be committed to your Git repository by accident. Removing this data from the history ensures that it is completely erased and cannot be recovered. The process is straightforward: simply remove the data from the current files and purge it from the commit history.

**Remove the Sensitive Data from Files:**  
Edit the files to remove the sensitive data.  
Stage the changes:  
`git add <file>`

**Commit the Changes:**  
Commit the changes to remove the sensitive data from the latest commit:  
`git commit -m "Remove sensitive data from files"`

**Use Git Filter-Repo:**  
`git filter-repo --path <filename> --invert-paths`  
`git filter-repo --replace-text <file-with-replacements>`

**Force Push the Changes:**  
Force push the rewritten history to the remote repository:  
`git push origin --force --all`  
`git push origin --force --tags`
