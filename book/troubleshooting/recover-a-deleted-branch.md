---
tag: done
---

# Recover a Deleted Branch

If you accidentally delete a branch, don't worry. Git provides a way to recover it if the branch hasn't been garbage collected. To do this, simply find the commit where the branch pointed before it was deleted and recreate the branch.

**Find the Commit Hash:**  
Use the reflog to find the commit hash of the deleted branch:  
`git reflog`

**Create a New Branch from the Commit:**  
Create a new branch using the commit hash:  
`git checkout -b <branch-name> <commit-hash>`
