---
tag: done
---

# Move Commits to a New Branch

Sometimes, you may realize that the commits you have made should be on a different branch. Move the commits to a new branch by creating a new branch from the current commit and then resetting the original branch to a previous commit.

**Create a New Branch:**  
Create a new branch from the current commit:  
`git branch <new-branch>`

**Reset the Original Branch:**  
Switch back to the original branch:  
`git checkout <original-branch>`

Reset the original branch to the commit before the commits you want to move:  
`git reset --hard <commit>`
