---
tag: done
---

# Undo Last Commit

Sometimes, you may need to undo the last commit because it contains mistakes or was made prematurely. Git provides several ways to undo the last commit, and you can choose whether you want to keep the changes in your working directory or discard them completely.

**Undo the Commit but Keep the Changes:**  
Use the following command to undo the last commit and keep the changes in the working directory:  
`git reset --soft HEAD~1`

**Undo the Commit and Discard the Changes:**  
Use the following command to undo the last commit and discard the changes:  
`git reset --hard HEAD~1`

**Undo the Commit but Keep the Changes Staged:**  
Use the following command to undo the last commit and keep the changes staged:  
`git reset --mixed HEAD~1`
