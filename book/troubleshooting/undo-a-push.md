---
tag: done
---

# Undo a Push

If you need to undo a push to a remote repository, you can do so by resetting the local branch to a previous commit and force-pushing the changes to the remote repository. This is necessary if you made a mistake in the commit or if you want to retract changes that were pushed prematurely.

**Find the Commit to Reset To:**  
Identify the commit you want to reset your branch to by using:  
`git log`

**Reset the Local Branch:**  
Reset your local branch to the chosen commit:  
`git reset --hard <commit>`

**Force Push the Reset Branch:**  
Force push the local branch to the remote repository:  
`git push origin <branch> --force`
