---
tag: done
---

# Troubleshooting

!include book/troubleshooting/remove-sensitive-data.md

\newpage

!include book/troubleshooting/undo-a-push.md

\newpage

!include book/troubleshooting/move-commit-to-correct-branch.md

\newpage

!include book/troubleshooting/resolve-merge-conflicts.md

\newpage

!include book/troubleshooting/delete-a-remote-branch.md

\newpage

!include book/troubleshooting/undo-a-merge.md

\newpage

!include book/troubleshooting/recover-a-deleted-branch.md

\newpage

!include book/troubleshooting/fix-a-rebase.md

\newpage

!include book/troubleshooting/squash-commits.md

\newpage

!include book/troubleshooting/use-git-reflog-to-recover-from-mistakes.md

\newpage

!include book/troubleshooting/amend-a-commit.md

\newpage

!include book/troubleshooting/move-commits-to-a-new-branch.md

\newpage

!include book/troubleshooting/undo-last-commit.md

\newpage

!include book/troubleshooting/fix--showing-no-changes.md

\newpage

!include book/troubleshooting/revert-a-commit-from-several-commits-ago.md

\newpage

!include book/troubleshooting/undo-changes-to-a-file.md

\newpage

!include book/troubleshooting/completely-reset-local-changes.md

\newpage

!include book/troubleshooting/remove-a-file-from-the-remote-repsitory.md

\newpage
