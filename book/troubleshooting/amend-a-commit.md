---
tag: done
---

# Amend a Commit

Amending a commit is the best way to make small corrections to a recent commit without creating a new one. This is useful for fixing typos in commit messages or adding forgotten changes.

**Stage the Changes:**  
If you need to modify the commit content, stage the new changes:  
`git add <file>`

**Amend the Last Commit:**  
Amend the most recent commit with the following command:  
`git commit --amend`

**Edit the Commit Message (if needed):**  
If you need to change the commit message, your text editor will open. Modify the message and save the changes.

**Force Push the Amended Commit (if already pushed):**  
If the commit has already been pushed to a remote repository, force push the changes:  
`git push origin <branch> --force`
