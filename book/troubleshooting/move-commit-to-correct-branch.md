---
tag: done
---

# Move Commit to Correct Branch

Sometimes, you may accidentally commit changes to the wrong branch. Don't worry, though. You can easily move a commit to the correct branch using a combination of `git checkout`, `git branch`, and `git cherry-pick` commands. This process involves creating a new branch, moving the commit, and updating the branches accordingly.

**Identify the Commit:**  
Find the commit hash of the commit you want to move:  
`git log`

**Create a New Branch from the Commit:**  
Create a new branch starting from the commit you want to move:  
`git branch <new-branch> <commit>`

**Checkout the Correct Branch:**  
Switch to the correct branch where the commit should be:  
`git checkout <correct-branch>`

**Cherry-Pick the Commit:**  
Apply the commit to the correct branch:  
`git cherry-pick <commit>`

**Remove the Commit from the Wrong Branch:**  
Switch back to the wrong branch and reset it to the previous commit:  
`git checkout <wrong-branch>`  
`git reset --hard HEAD~1`
