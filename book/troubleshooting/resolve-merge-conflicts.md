---
tag: done
---

# Resolve Merge Conflicts

Merge conflicts occur when Git cannot automatically merge changes from different branches. This typically happens when changes to the same lines of code are made in different branches. The solution is to resolve merge conflicts manually by editing the conflicting files to reconcile the differences.

**Identify Conflicted Files:**  
Use the following command to see which files have conflicts:  
`git status`

**Open the Conflicted Files:**  
Open each conflicted file in a text editor. Conflicted sections are marked by Git with conflict markers.

**Edit the Conflicted Sections:**  
Manually resolve the conflicts by editing the sections between the conflict markers (`<<<<<<<`, `=======`, `>>>>>>>`).

**Stage the Resolved Files:**  
After resolving the conflicts, stage the changes:  
`git add <file>`

**Complete the Merge:**  
Commit the merge to complete the process:  
`git commit`
