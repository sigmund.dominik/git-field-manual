---
tag: done
---

# Use Git Reflog to Recover from Mistakes

Git reflog is a powerful tool that records when the tips of branches and other references were updated. It allows you to recover from mistakes by providing a history of changes, even if those changes are not in the current branch history. Use it to your advantage.

**View the Reflog:**  
Display the reflog to see a history of changes:  
`git reflog`

**Identify the Desired State:**  
Look through the reflog entries to find the commit hash of the state you want to recover.

**Reset to the Desired State:**  
Reset your branch to the desired state using the commit hash:  
`git reset --hard <commit-hash>`
