---
tag: done
---

# Revert a Commit from Several Commits Ago

Reverting a commit from several commits ago is a simple process that involves creating a new commit that undoes the changes introduced by the previous commit. This process preserves the history of your project while removing the specific changes made by the target commit.

**Identify the Commit:**  
Use the following command to find the commit hash:  
`git log`

**Revert the Commit:**  
Revert the specified commit using:  
`git revert <commit-hash>`

**Resolve Any Conflicts:**  
If there are conflicts, resolve them and then commit the changes:  
`git commit -m "Resolve conflicts"`
