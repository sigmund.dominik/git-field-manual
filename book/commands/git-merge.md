---
tag: done
---

# git merge

The `git merge` command is the tool you need to combine changes from one branch into another. It's the key to integrating the work done on different branches, and it's essential for any collaborative workflow in Git.

`git merge <branch-name>`  
Merges the specified branch into the current branch.

`git merge --no-ff <branch-name>`  
Performs a merge commit even if the merge could be resolved with a fast-forward. This helps to keep a history of the merge in the commit log.

`git merge --squash <branch-name>`  
Merges changes from the specified branch but does not create a merge commit. Instead, it stages the changes, allowing you to create a single commit.

`git merge --abort`  
Aborts the merge process and attempts to reconstruct the pre-merge state.

`git merge --continue`  
Continues a merge after conflicts have been resolved.

`git merge --no-commit`  
Performs the merge but does not automatically create a commit, allowing you to inspect the merged state.
