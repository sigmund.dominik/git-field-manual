---
tag: done
---

# git add

Use the `git add` command to add changes in the working directory to the staging area. This is the first step in the Git workflow. It allows you to stage changes before committing them. This command tells Git that you want to include updates to a particular file or directory in the next commit, track new files, or mark files for deletion.

`git add <file>`  
Stages changes for a specific file.

`git add <directory>`  
Stages changes for all files in the specified directory.

`git add .`  
Stages all changes in the current directory and its subdirectories.

`git add -A`  
Stages all changes in the entire working directory, including deletions.

`git add -p`  
Stages changes interactively, allowing you to review and selectively stage hunks of changes.

`git add -u`  
Stages changes to tracked files, excluding new files that are not yet tracked.
