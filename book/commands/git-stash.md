---
tag: done
---

# git stash

The `git stash` command is used to temporarily save changes in your working directory that you are not ready to commit. It allows you to clean your working directory without losing your changes, so you can work on something else and come back to your stashed changes later.

`git stash`  
Stashes the current changes in your working directory and index.

`git stash list`  
Lists all stashed changes along with their unique identifiers and messages.

`git stash apply`  
Applies the latest stashed changes to your working directory without removing them from the stash list.

`git stash apply <stash>`  
Applies the specified stash to your working directory without removing it from the stash list.

`git stash pop`  
Applies the latest stashed changes and removes them from the stash list.

`git stash pop <stash>`  
Applies and removes the specified stash.

`git stash drop`  
Removes the latest stash from the stash list without applying it.

`git stash drop <stash>`  
Removes the specified stash without applying it.

`git stash clear`  
Removes all stashes.

`git stash save "<message>"`  
Stashes the changes with a custom message for easier identification.

`git stash show`  
Shows the changes in the latest stash.

`git stash show <stash>`  
Shows the changes in the specified stash.

`git stash branch <branchname>`  
Creates a new branch and applies the latest stash to it.

`git stash branch <branchname> <stash>`  
Creates a new branch and applies the specified stash to it.
