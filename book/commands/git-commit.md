---
tag: done
---

# git commit

The `git commit` command is used to save your changes to the local repository. It captures a snapshot of the project's currently staged changes, along with a log message from the user describing the changes. This command is essential for recording the history of your project.

`git commit -m "<message>"`  
Commits the staged changes with the specified commit message.

`git commit -a -m "<message>"`  
Automatically stages all tracked, modified files before committing, then commits with the specified message.

`git commit --amend`  
Amends the last commit, allowing you to modify the commit message or include additional changes.

`git commit --verbose`  
Shows the diff of the changes being committed in the commit message editor.

`git commit --all`  
Stages and commits all changes to tracked files in one step.

`git commit --no-verify`  
Bypasses pre-commit and commit-msg hooks. Use with caution.
