---
tag: done
---

# git reflog

Use the `git reflog` command to view the reference logs, which record when the tips of branches and other references were updated in the repository. This includes changes made by commits, rebases, resets, and branch deletions. It is a valuable tool for recovering lost commits and branches.

`git reflog`  
Displays the reflog for the current branch.

`git reflog show <ref>`  
Displays the reflog for the specified reference (e.g., branch, tag).

`git reflog expire --expire=<time>`  
Expires reflog entries older than the specified time.

`git reflog delete <ref>`  
Deletes the specified reflog entry.

`git reflog --date=relative`  
Shows the dates in a relative format (e.g., "2 days ago").
