---
tag: done
---

# git pull

The `git pull` command is used to fetch and integrate changes from a remote repository into your current branch. It combines the actions of `git fetch` and `git merge`, allowing you to update your local repository with changes from the remote repository in one step. This command ensures your local branch is up-to-date with the remote branch.

`git pull origin main`
Fetches changes from the `main` branch of the `origin` remote repository and merges them into the current branch.

`git pull`  
Fetches and merges changes from the default remote repository and its configured branch.

`git pull <remote>`  
Fetches and merges changes from the specified remote repository, usually named `origin`.

`git pull <remote> <branch>`  
Fetches and merges changes from the specified branch of the remote repository.

`git pull --rebase`  
Rebase the current branch on top of the upstream branch after fetching.

`git pull --ff-only`  
Only perform fast-forward merges; fail if a fast-forward merge is not possible.

`git pull --no-commit`  
Fetch and merge changes without committing the merge result, allowing you to inspect the changes.

`git pull --squash`  
Squash all fetched commits into a single commit before merging.
