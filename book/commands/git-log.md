---
tag: done
---

# git log

The `git log` command is used to view the commit history of a repository. It displays a list of commits, showing details such as commit hash, author, date, and commit message. This command is essential for tracking changes and understanding the project's development over time.

`git log`  
Displays the commit history in reverse chronological order.

`git log --oneline`  
Shows a simplified, one-line summary of each commit, showing only the commit hash and the commit message. Useful for a quick overview.

`git log --graph`  
Displays the commit history as a graph, showing branch and merge relationships.

`git log --decorate`  
Adds branch and tag names to the commit log.

`git log --stat`  
Shows the file statistics (insertions, deletions) for each commit.

`git log -p`  
Displays the patch (diff) introduced in each commit.

`git log --author="<author>"`  
Filters commits by a specific author.

`git log --since="<date>" --until="<date>"`  
Filters commits by date range.

`git log --grep="<pattern>"`  
Searches commit messages for a specific pattern.

`git log --all`  
Displays the commit history of all branches.
