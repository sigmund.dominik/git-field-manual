---
tag: done
---

# git tag

The `git tag` command is used to create, list, and manage tags in Git. Tags are a way to mark specific points in your repository’s history, typically used to identify important commits such as release points.

Pushing a tag to the remote repository with `git push <remote> <tagname>` makes the tag available to collaborators. The `--tags` option pushes all local tags to the remote repository, ensuring that all marked points are shared.

`git tag`  
Lists all tags in the repository, providing an overview of marked points in your project’s history.

`git tag <tagname>`  
Creates a lightweight tag with the specified name, e.g., `v1.0`, which is simply a named pointer to a commit.

`git tag -a <tagname> -m "<message>"`  
Creates an annotated tag with the specified name and message, including additional metadata such as the tagger’s name, email, and date.

`git tag -d <tagname>`  
Deletes the specified tag from the local repository. Note that this does not delete the tag from the remote repository.

`git tag -l "<pattern>"`  
Lists tags that match the specified pattern.

`git tag -f <tagname>`  
Forces the creation of a tag, overwriting an existing tag with the same name if necessary.

`git tag <tagname> <commit>`  
Creates a tag for the specified commit.

`git push <remote> <tagname>`  
Pushes a specific tag to the remote repository.

`git push <remote> --tags`  
Pushes all tags to the remote repository.
