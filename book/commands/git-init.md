---
tag: done
---

# git init

The `git init` command is used to create a new, empty Git repository or to reinitialize an existing one. It sets up the necessary files and directories for Git to manage version control in a project. This command is the starting point for a new project under Git's version control. It creates a `.git` directory in the project root, which contains all the metadata and configuration needed for version control.

If the directory already contains a Git repository, running `git init` will reinitialize it, but it will not overwrite existing data.

This command is fundamental for starting to track changes in a new project or to place an existing project under version control.

`git init`
Initializes a new Git repository in the current directory. This creates a `.git` directory containing all the necessary metadata for the repository.

`git init <directory>`
Initializes a new Git repository in the specified directory. If the directory does not exist, Git will create it.
