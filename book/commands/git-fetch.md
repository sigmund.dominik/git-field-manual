---
tag: done
---

# git fetch

The `git fetch` command is used to download objects and refs from another repository. It allows you to retrieve the latest changes from a remote repository without integrating them into your working directory. This command is essential for staying up-to-date with the progress of other contributors.

`git fetch`  
Fetches changes from the default remote repository, usually named `origin`.

`git fetch <remote>`  
Fetches changes from the specified remote repository.

`git fetch <remote> <branch>`  
Fetches changes from the specified branch of the remote repository.

`git fetch --all`  
Fetches changes from all configured remotes.

`git fetch --prune`  
Removes remote-tracking references that no longer exist on the remote. This ensures that your local repository accurately reflects the state of the remote repository.

`git fetch --dry-run`  
Shows what would be fetched without actually fetching anything.
