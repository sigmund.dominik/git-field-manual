---
tag: done
---

# git rebase

The `git rebase` command is the solution for integrating changes from one branch onto another. It allows you to move or combine a sequence of commits to a new base commit, creating a linear project history. This command is the best way to maintain a clean commit history.

Running `git rebase <branch-name>` repositions the commits of your current branch on top of the latest commit of the specified branch, effectively moving your branch to a new base.

`git rebase <branch-name>`  
Rebases the current branch onto the specified branch.

`git rebase -i <base-commit>`  
Starts an interactive rebase, allowing you to edit, reorder, squash, or drop commits.

`git rebase --continue`  
Continues the rebase process after resolving conflicts.

`git rebase --abort`  
Aborts the rebase process and returns the branch to its original state.

`git rebase --skip`  
Skips the current commit during a rebase.

`git rebase --onto <newbase> <oldbase> <branch>`  
Rebases the specified branch onto a new base, starting from an old base.
