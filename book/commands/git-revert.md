---
tag: done
---

# git revert

Use the `git revert` command to create a new commit that undoes the changes introduced by a previous commit. Unlike `git reset`, which changes the history of a repository, `git revert` maintains the history and adds a new commit to undo the specified changes. This is the best way to undo changes in a safe manner, especially in a shared repository.

`git revert <commit>`  
Reverts the specified commit by creating a new commit that undoes its changes.

`git revert -n <commit>` or `git revert --no-commit <commit>`  
Applies the changes introduced by the specified commit in the working directory and staging area without committing them, allowing you to review and make additional changes.

`git revert -m <parent-number> <merge-commit>`  
Reverts a merge commit by specifying the parent number.

`git revert --continue`  
Continues the revert process after resolving conflicts.

`git revert --abort`  
Aborts the revert process and attempts to reconstruct the pre-revert state.

`git revert --quit`  
Stops the revert process, keeping the index and working directory in their current state.
