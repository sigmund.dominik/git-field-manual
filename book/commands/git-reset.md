---
tag: done
---

# git reset

The `git reset` command is used to undo changes in your working directory and staging area. It allows you to move the current branch pointer to a specified commit, optionally modifying the index and working directory to match. This powerful command can undo changes, but use it with care to avoid losing work.

`git reset <commit>`  
Resets the current branch to the specified commit, leaving the working directory and index unchanged (default mode).

`git reset --soft <commit>`  
Moves the current branch to the specified commit, leaving the index and working directory unchanged. All changes remain staged.

`git reset --mixed <commit>`  
Resets the current branch to the specified commit and updates the index, but leaves the working directory unchanged. This is the default mode.

`git reset --hard <commit>`  
Resets the current branch to the specified commit and updates both the index and working directory to match the commit. All changes are lost.

`git reset --keep <commit>`  
Resets the current branch to the specified commit, preserving uncommitted local changes that do not conflict with the reset.

`git reset <file>`  
Unstages the specified file(s), keeping the changes in the working directory.
