---
tag: done
---

# git checkout

The `git checkout` command is the key to switching between branches and restoring working directory files. It is an essential command for navigating your project's history and managing different lines of development.

`git checkout <branch-name>`  
Switches to the specified branch.

`git checkout -b <new-branch-name>`  
Creates a new branch and switches to it.

`git checkout <commit-hash>`  
Checks out a specific commit, putting the repository in a "detached HEAD" state. This means your HEAD is not attached to any branch, but directly to the commit. This can be useful for inspecting old commits or creating temporary builds.

`git checkout -- <file>`  
Discards changes in the working directory for the specified file, restoring it to the last commit.

`git checkout .`  
Discards changes in all files in the working directory, restoring them to the last commit.

`git checkout <branch-name> -- <file>`  
Checks out a specific file from another branch and writes it into the working directory.
