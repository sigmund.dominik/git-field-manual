---
tag: done
---

# git diff

The `git diff` command is used to show changes between commits, branches, or the working directory and the staging area. It helps you see what has been modified, added, or deleted before committing your changes. This command is essential for reviewing changes and understanding their impact on the codebase.

`git diff`  
Shows changes between the working directory and the staging area.

`git diff --staged`  
Shows changes between the staging area and the last commit.

`git diff <commit>`  
Shows changes between the working directory and the specified commit.

`git diff <commit1> <commit2>`  
Shows changes between two commits.

`git diff <branch1> <branch2>`  
Shows changes between two branches.

`git diff <file>`  
Shows changes in a specific file.

`git diff --name-only`  
Shows the names of changed files without showing the actual changes.

`git diff --name-status`  
Shows the names and statuses (added, modified, deleted) of changed files.

`git diff --stat`  
Shows a summary of changes, including the number of insertions and deletions for each file.

`git diff --color`  
Highlights changes using color for better readability.
