---
tag: done
---

# git remote

The `git remote` command is the tool you need to manage the set of remote repositories ("remotes") whose branches you track. It allows you to add, remove, and interact with remote repositories, enabling collaboration and synchronization of work across different repositories.

`git remote`  
Lists the short names of all remote repositories.

`git remote -v`  
Lists the remote repositories along with their URLs.

`git remote add <name> <url>`  
Adds a new remote repository with the specified name and URL, allowing you to fetch from and push to this  repository.

`git remote remove <name>`  
Removes the specified remote repository from your configuration.

`git remote rename <old-name> <new-name>`  
Changes the name of an existing remote repository.

`git remote set-url <name> <newurl>`  
Changes the URL of the specified remote repository.

`git remote get-url <name>`  
Retrieves the URL of the specified remote repository.

`git remote show <name>`  
Displays detailed information about the specified remote repository, including its branches and tracking status.

`git remote prune <name>`  
Deletes stale references to remote branches that have been removed from the remote repository.

`git remote update`  
Fetches the latest changes from all remote repositories.
