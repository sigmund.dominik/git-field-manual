---
tag: done
---

# git clone

The `git clone` command is used to create a copy of an existing Git repository. This command downloads the repository, including its entire history and all branches, from a remote location to your local machine. It is commonly used to retrieve a repository hosted on a platform like GitHub, GitLab, or Bitbucket.

This command is essential for obtaining a working copy of a remote repository and setting up local development or contributing to a project.

`git clone <repository_url>`  
Clones the repository at the specified URL into a new directory named after the repository.

`git clone <repository_url> <directory>`  
Clones the repository at the specified URL into the specified local directory.

`git clone --bare <repository_url>`  
Clones the repository in a "bare" format, which is typically used for creating a mirror of the repository.

`git clone --mirror <repository_url>`  
Similar to `--bare`, but also includes all refs and config settings from the source repository.

`git clone --branch <branch> <repository_url>`  
Clones a specific branch from the repository.

`git clone --depth <depth> <repository_url>`  
Performs a shallow clone with a history truncated to the specified number of commits.
