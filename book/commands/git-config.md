---
tag: done
---

# git config

The `git config` command is used to set configuration options for your Git installation. These configurations can be applied at three different levels: local, global, and system. It allows you to customize Git’s behavior and preferences for your repositories.

`git config --global user.name "Your Name"`  
Sets the user name for all repositories on the current system.

`git config --global user.email "your.email@example.com"`  
Sets the user email for all repositories on the current system.

`git config --local`  
Sets configuration options for the current repository. This is the default behavior.

`git config --global`  
Applies the configuration to the user’s global Git configuration, affecting all repositories for the user.

`git config --system`  
Applies the configuration at the system level, affecting all users on the machine.

`git config --list`  
Lists all the configuration settings, showing the effective configuration values.

`git config <key> <value>`  
Sets the specified configuration key to the given value.

`git config --unset <key>`  
Removes the specified configuration key.

`git config --edit`  
Opens the configuration file in the default text editor for manual editing.

`git config --get <key>`  
Retrieves the value of the specified configuration key.

`git config --global --unset-all <key>`  
Removes all occurrences of a configuration key in the global config file.

`git config --system --unset-all <key>`  
Removes all occurrences of a configuration key in the system config file.
