---
tag: done
---

# git status

The `git status` command is the best way to see what's going on in your working directory and staging area. It shows you which changes have been staged, which haven't, and which files aren't being tracked by Git. This command helps you understand the current state of your project and what actions you need to take next.

It provides a detailed summary of the current state of your working directory and the staging area. This includes:

- **Untracked Files**: Files that are not yet being tracked by Git.
- **Changes Not Staged for Commit**: Modifications that have been made but not yet staged.
- **Changes to be Committed**: Changes that have been staged and will be included in the next commit.

`git status`  
check the status of your repository

`git status -s` or `git status --short`  
Provides a compact, one-line output for each file, showing its status, using symbols to indicate the status of each file (e.g., `A` for added, `M` for modified, `??` for untracked).

`git status -b` or `git status --branch`  
Shows the branch information and the tracking status.

`git status --ignored`  
Shows the ignored files as well, based on the `.gitignore` file.
