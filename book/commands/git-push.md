---
tag: done
---

# git push

The `git push` command is the key to sharing your work with others and collaborating on a project. It uploads your local repository content to a remote repository, transferring commits from your local branch to a branch on a remote repository. This ensures that others can access your changes and keeps your local repository in sync with the remote repository.

`git push origin main`
Pushes changes from the `main` branch of the local repository to the `main` branch of the `origin` remote repository.

`git push`  
Pushes changes from the local repository to the default remote repository and its configured branch.

`git push <remote>`  
Pushes changes to the specified remote repository, usually named `origin`.

`git push <remote> <branch>`  
Pushes changes from the local branch to the specified branch of the remote repository.

`git push --all`  
Pushes all branches to the remote repository, updating all remote branches to match the local repository.

`git push --tags`  
Pushes all tags to the remote repository.

`git push <remote> --delete <branch>`  
Deletes the specfied branch on the remote repository.

`git push --force`  
Forces the push, potentially overwriting changes on the remote branch. Use with caution.

`git push --set-upstream <remote> <branch>`  
Sets the upstream branch for the current branch, establishing a tracking relationship between the local and remote branches.
