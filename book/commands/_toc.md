---
tag: done
---
# Commands

!include book/commands/git-init.md

\newpage

!include book/commands/git-clone.md

\newpage

!include book/commands/git-status.md

\newpage

!include book/commands/git-add.md

\newpage

!include book/commands/git-commit.md

\newpage

!include book/commands/git-log.md

\newpage

!include book/commands/git-diff.md

\newpage

!include book/commands/git-branch.md

\newpage

!include book/commands/git-checkout.md

\newpage

!include book/commands/git-merge.md

\newpage

!include book/commands/git-rebase.md

\newpage

!include book/commands/git-fetch.md

\newpage

!include book/commands/git-pull.md

\newpage

!include book/commands/git-push.md

\newpage

!include book/commands/git-tag.md

\newpage

!include book/commands/git-reset.md

\newpage

!include book/commands/git-revert.md

\newpage

!include book/commands/git-stash.md

\newpage

!include book/commands/git-remote.md

\newpage

!include book/commands/git-config.md

\newpage

!include book/commands/git-reflog.md

\newpage

