---
tag: done
---

# git branch

The `git branch` command is the tool you need to manage branches in your Git repository. It lets you create, list, rename, and delete branches. Branches are essential for isolating work on different features, bug fixes, or experiments in your project.

`git branch`  
Lists all branches in the repository.

`git branch <branch-name>`  
Creates a new branch with the specified name.

`git branch -d <branch-name>`  
Deletes the specified branch. Git will prevent you from deleting a branch that has unmerged changes.

`git branch -D <branch-name>`  
Force-deletes the specified branch, even if it has unmerged changes.

`git branch -m <old-branch-name> <new-branch-name>`  
Renames a branch from `<old-branch-name>` to `<new-branch-name>`

`git branch --show-current`  
Displays the name of the current branch.

`git branch -r`  
Lists only the remote-tracking branches.

`git branch -a`  
Lists all local and remote branches.

`git branch --merged`  
Lists branches that have been merged into the current branch.

`git branch --no-merged`  
Lists branches that have not been merged into the current branch.
