---
tag: done
---

# Archive

Use the Git archive command to create an archive file of a specific version of your repository. This is the best way to generate distribution packages of your project without including the entire Git history.

Creating an Archive:  
`git archive --format=tar --output=<file.tar> <branch-or-commit>`
