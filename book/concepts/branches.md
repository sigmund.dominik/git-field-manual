---
tag: done
---

# Branches

Branches are the key to developing features, fixing bugs, and safely experimenting with new ideas in isolation from the main codebase. They enable parallel development and collaboration among team members.

A branch is a separate line of development within a Git repository. Git creates a branch named `main`, which is considered the main branch by default. New branches can be created to work on different tasks without affecting the main branch.

- Create a Branch: `git branch <branch-name>`
- Switch to a Branch: `git checkout <branch-name>`
- Create and Switch: `git checkout -b <branch-name>`
- Merge a Branch: `git merge <branch-name>` (after switching to the target branch)
- Delete a Branch: `git branch -d <branch-name>` or `git branch -D <branch-name>`

```{.mermaid theme=neutral}
gitGraph
    commit
    branch feature
    checkout feature
    commit
    checkout main
    merge feature
    commit
```
