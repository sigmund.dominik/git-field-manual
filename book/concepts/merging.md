---
tag: done
---

# Merging

Merging is the process of combining changes from different branches into a single branch. It is a fundamental part of Git's branching model, and it enables collaboration and parallel development.

- **Fast-Forward Merge**: Moves the branch pointer forward if there are no divergent commits.
- **Three-Way Merge**: Combines changes from the common ancestor, current branch, and the branch being merged.

Merge conflicts occur when changes in different branches overlap. Git will pause the merge process and allow you to resolve the conflicts manually.
