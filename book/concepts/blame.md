---
tag: done
---

# Blame

Git blame is an invaluable command for identifying the author of specific lines within a file. It helps track changes and understand the history of a file by showing who made each change and when.

Git blame annotates each line in a file with information about the commit that last modified the line. This information includes the commit hash, author, date, and the line number.
