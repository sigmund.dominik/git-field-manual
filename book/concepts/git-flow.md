---
tag: done
---

# Git Flow

Git Flow is the definitive branching model for Git, created by Vincent Driessen. It provides a structured process for managing feature development, releases, and hotfixes. It defines a set of clear guidelines for creating, merging, and deleting branches in a consistent manner.

Git Flow uses five main branch types:

1. **main**: The main branch where the source code of HEAD always reflects a production-ready state.
2. **develop**: The branch where the source code of HEAD always reflects a state with the latest delivered development changes for the next release.
3. **feature**: Branches created from `develop` for working on new features.
4. **release**: Branches created from `develop` to prepare for a new production release.
5. **hotfix**: Branches created from `master` to quickly patch production releases.

```{.mermaid theme=neutral}
gitGraph
    commit
    branch release
    branch develop
    commit
    branch feature
    checkout feature
    commit
    checkout develop
    merge feature
    checkout main
    merge develop
    branch hotfix
    checkout hotfix
    commit
    checkout main
    merge hotfix
    checkout develop
    merge hotfix
    checkout release
    commit
    merge main
    checkout develop
    merge release
```
