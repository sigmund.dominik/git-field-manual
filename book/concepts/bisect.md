---
tag: done
---

# Bisect

Git bisect is the tool you need to find the specific commit that introduced a bug or issue in your code. It uses a binary search algorithm to efficiently narrow down the range of commits, identifying the problematic one.

Here's how to use Git bisect:

1. **Start Bisecting:** Begin the bisecting process by marking the current commit as bad (contains the bug) and specifying an older commit as good (known to be bug-free).
2. **Binary Search:** Git will automatically select a commit between the good and bad commits for you to test.
3. **Test the Commit:** Check if the commit is good or bad. Based on the result, mark the commit as good or bad.
4. **Repeat:** Git will continue selecting commits for testing until it identifies the commit that introduced the bug.
5. **Finish Bisecting:** Once Git identifies the problematic commit, it will display the commit hash and details.
6. **Reset Bisect:** Reset the bisecting process to return to the original state of the repository.