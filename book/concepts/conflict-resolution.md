---
tag: done
---

# Conflict Resolution

Conflict resolution is an essential skill in Git, as it allows you to manage and resolve situations where changes in different branches overlap. Git will alert you to conflicts during merges or rebases, and you'll need to address these conflicts manually.

When a conflict occurs, Git will pause the merge or rebase process and mark the conflicting areas in the affected files with conflict markers (`<<<<<<`, `======`, `>>>>>>`).

Example of a Conflict Marker:

```plaintext
<<<<<<< HEAD
Changes from the current branch.
=======
Changes from the branch being merged.
>>>>>>> feature-branch
```

- Identify conflicts ( `<<<<<<`, `======`, and `>>>>>>`)
- edit files (remove conflict markers and resolve conflicts)
- stage changes (`git add`)
- complete the process (commit, merge, or rebase)
