---
tag: done
---

# Repositories

A Git repository (or "repo") is the virtual storage of your project. It allows you to save versions of your code, which you can access when needed.

**Local Repository:** A repository on your personal computer.  
**Remote Repository:** A repository hosted on the internet or a network, enabling collaboration.

Creating a new Repository:  
`git init`

Cloning an existing Repository:  
`git clone <url>`

A Git repository consists of three main parts:

**Working Directory**: The current state of your project files.  
**Staging Area (Index)**: A place to keep changes that will be part of the next commit.  
**Git Directory (.git)**: The repository's database, containing all the version history.

```{.mermaid theme=neutral}
graph TD;
    A[Working Directory] -->|git add| B[Staging Area];
    B -->|git commit| C[Local Repository];
    C -->|git push| D[Remote Repository];
    D -->|git fetch| C;
    D -->|git pull| A;
```
