---
tag: done
---

# Cherry-Picking

Cherry-picking in Git allows you to apply specific changes from an existing commit onto your current branch. This is useful for selectively integrating specific changes without merging entire branches.

Cherry-picking is the process of picking a commit from one branch and applying it to another. Unlike merging, which integrates all changes from one branch into another, cherry-picking allows you to choose specific commits to apply.

When to Use Cherry-Picking:

- When you need a specific fix or feature from another branch.
- When you want to apply a single commit to multiple branches.
- When you want to avoid merging unrelated changes.

To cherry-pick a commit, you need the commit hash (SHA-1). Use the `git cherry-pick` command followed by the commit hash:  
`git cherry-pick <commit-hash>`

This command will apply the changes introduced by the specified commit to your current branch.
