---
tag: done
---

# Rebasing

Rebasing is a powerful tool in Git that allows you to integrate changes from one branch into another by moving or combining a sequence of commits. This process creates a linear, more readable project history.

Rebasing is the process of moving or combining a sequence of commits to a new base commit. It is typically used to:

- Update a feature branch with changes from the main branch.
- Clean up commit history by combining or reordering commits.

Interactive rebase allows you to modify commits in various ways (reword, squash, edit, etc.) as they are rebased. It is useful for cleaning up commit history before merging.  
`git rebase -i <base-commit>`

A standard rebase moves the entire branch to start from a different commit, usually the tip of the main branch.  
`git checkout feature-branch`  
`git rebase main`
