---
tag: done
---

# Hooks

Git hooks are scripts that run automatically in response to specific events in a Git repository. They allow you to automate tasks and enforce policies, enhancing your workflow and ensuring code quality.

- **Client-Side Hooks:** Run on your local machine in response to operations like committing and merging.
- **Server-Side Hooks:** Run on the server in response to operations like receiving pushed commits.

They are stored in the `.git/hooks` directory of your repository and are not shared with other users when you push or clone a repository.

Common Client-Side Hooks:

- **Pre-Commit**: Runs before a commit is created. Use this hook to check code quality, run tests, or ensure commit message standards.
- **Commit-Msg Hook**: Runs after the commit message is entered. Use this hook to validate or format the commit message.
- **Pre-Push Hook**:  Runs before changes are pushed to a remote repository. Use this hook to run tests or check for certain conditions before pushing.

Common Server-Side Hooks:

- **Pre-Receive Hook**: Runs on the remote server before any changes are accepted. Use this hook to enforce project policies, such as commit message guidelines or branch protections.
- **Update Hook**: Runs after the pre-receive hook but before the changes are applied. It can be used to check specific refs (e.g., branches or tags).
- **Post-Receive Hook**: Runs after the changes have been applied. Use this hook to trigger continuous integration pipelines or deployment scripts.
