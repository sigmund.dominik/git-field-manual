---
tag: done
---

# Tags

Tags in Git are used to mark specific points in history as being important. Tags are typically used to mark release points (e.g., v1.0, v2.0). There are two types of tags in Git:

**Lightweight tags** are pointers to a specific commit. They are quick and simple to create but do not contain additional metadata.

**Annotated tags** are stored as full objects in the Git database. They contain a tagger name, email, date, and a message, and can be signed with GPG.

`git tag <tag-name>`  
Create a lightweight tag:

`git tag -a <tag-name> -m "Tag message"`  
Create an annotated tag

`git tag`  
List all tags
