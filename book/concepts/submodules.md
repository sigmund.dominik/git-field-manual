---
tag: done
---

# Submodules

Submodules are an essential tool for managing and incorporating external projects or libraries into your repository. They allow you to maintain their own version history while incorporating them into your project.

A submodule is a Git repository embedded inside another Git repository. The main repository holds a reference to a specific commit in the submodule, allowing you to treat the submodule as a standalone project.

Adding a Submodule:  
`git submodule add <repository_url> <directory>`  


When you clone a repository that contains submodules, you need to initialize and update the submodules:  
`git clone <repository_url>`  
`git submodule update --init --recursive`

To update the submodule to the latest commit in the submodule repository:  
`git submodule update --remote`

When you make changes in a submodule, you need to commit them in the submodule repository and then update the reference in the main repository: