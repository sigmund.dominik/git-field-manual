---
tag: done
---

# Commits

A commit is a snapshot of your project’s changes. It records the current state of your project, allowing you to track and manage the evolution of your code over time.

A commit typically consists of the following parts:

- **Commit Hash:** A unique identifier (SHA-1 hash) for the commit.
- **Author:** The person who made the changes.
- **Date:** The timestamp of when the commit was created.
- **Commit Message:** A description of the changes made in the commit.
- **Parent Commits:** References to the immediate predecessor commit(s), allowing Git to maintain the commit history.

Creating a Commit:

- Stage your Changes using `git add`
- Commit your Changes using `git commit -m "Your commit message"`
