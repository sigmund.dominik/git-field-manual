---
tag: done
---

# Installing Git

Before you can start using Git, you need to install it on your computer. This chapter provides a brief, straightforward guide on how to install Git on different operating systems.

## Installing Git on Windows

**Download the Installer**  
Go to the official Git website: [git-scm.com](https://git-scm.com/)  
Download the latest version of Git for Windows.  

**Run the Installer**  
Open the downloaded `.exe` file.

## Installing Git on macOS

**Using Homebrew**  
`brew install git`

**Using Xcode Command Line Tools**  
`xcode-select --install`

## Installing Git on Linux

**Debian/Ubuntu**  
`sudo apt update && sudo apt install git`

**Fedora**  
`sudo dnf install git`

**Arch Linux**  
`sudo pacman -S git`

## Verify the Installation

`git --version`  
To verify that Git has been installed correctly. You will see the installed version number.

## Configuring Git

Once you've installed Git, you must configure it with your name and email address. These details will be associated with your commits.

**Set your username:**  
`git config --global user.name "Your Name"`

**Set your email address:**  
`git config --global user.email "Your E-Mail"

**Verify the configuration:**  
`git config --global --list`
